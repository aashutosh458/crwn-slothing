import React, { Component } from 'react'
import { Route, Routes, Navigate } from "react-router-dom";

// Pages import 
import Header from "./Components/Header/Header";
import HomePage from './Pages/HomePage/HomePage';
import ShopPage from "./Pages/ShopPage/ShopPage";
import AuthPage from "./Pages/AuthPage/AuthPage";
import Checkout from './Pages/checkout/checkout';
import Collection from './Pages/Collection/Collection';
import Pr from './Components/Pr/Pr';

// Firebase import 
import { auth, createUserProfileDocument } from "./Firebase/firebase.utils";

// Redux import 
import { connect } from "react-redux";
import { setCurrentUser } from "./redux/user/userAction";
import { selectCurrentUser } from './redux/user/userSelector';
import { createStructuredSelector } from 'reselect';

import './App.css';

class App extends Component {

  unSubscribeFromAuth = null;

  componentDidMount() {
    const { setCurrentUser } = this.props
    this.unSubscribeFromAuth = auth.onAuthStateChanged(async userAuth => {
      if (userAuth) {
        const userRef = await createUserProfileDocument(userAuth);
        userRef.onSnapshot(snapshot => {
          setCurrentUser({
            id: snapshot.id,
            ...snapshot.data()
          });
        });
      }
      else {
        setCurrentUser(userAuth);
      }
    })
  }

  componentWillUnmount() {
    this.unSubscribeFromAuth();
  }
  render() {
    return (
      <>
        < Header />
        <Routes>
          <Route exact path='/' element={<HomePage />} />
          <Route path='/shop' element={<ShopPage />} />
          <Route path='/shop/:id' element={<Collection />} />
          <Route exact path='/auth' Component={() => this.props.currentUser ? (<Navigate to='/' />) : (<AuthPage />)} />
          <Route exact path='/checkout' element={<Checkout />} />
          <Route exact path='*' Component={Pr} />
        </Routes>
      </>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  currentUser: selectCurrentUser
});

const mapDispatchToProps = dispatch => (
  {
    setCurrentUser: user => dispatch(setCurrentUser(user))
  }
)

export default connect(mapStateToProps, mapDispatchToProps)(App);





//ch-8 v-22




// class App extends Component {
//   constructor() {
//     super();
//     this.state = {
//       currentUser: null
//     }
//   }

//   unSubscribeFromAuth = null;

//   componentDidMount() {
//     this.unSubscribeFromAuth = auth.onAuthStateChanged(async userAuth => {
//       if (userAuth) {
//         const userRef = await createUserProfileDocument(userAuth);
//         userRef.onSnapshot(snapshot => {
//           this.setState({
//             currentUser: {
//               id: snapshot.id,
//               ...snapshot.data()
//             }
//           })
//         })
//       }
//       else {
//         this.setState({ currentUser: userAuth });
//       }
//     })
//   }

//   componentWillUnmount() {
//     this.unSubscribeFromAuth();
//   }

//   render() {
//     return (
//       <>
//         <Header currentUser={this.state.currentUser} />
//         <Routes>
//           <Route exact path='/' Component={HomePage} />
//           <Route exact path='/shop' Component={ShopPage} />
//           <Route exact path='/auth' Component={AuthPage} />
//           <Route exact path='/pr' Component={pr} />
//         </Routes>
//       </>
//     );
//   }
// }


// export default App;
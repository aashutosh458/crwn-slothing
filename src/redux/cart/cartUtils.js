export const addItemToCart = (cartItems, cartItemToAdd) => {
  const existingCartItem = cartItems.find(cartItem =>
    cartItem.id === cartItemToAdd.id
  );

  if (existingCartItem) {
    return cartItems.map(cartItem =>
      cartItem.id === cartItemToAdd.id ?
        { ...cartItem, qty: cartItem.qty + 1 } :
        cartItem
    );
  }
  else {
    return [...cartItems, { ...cartItemToAdd, qty: 1 }];
  }
};

export const removeItem = (cartItems, item) => {
  return cartItems.filter(cartItem => cartItem.id !== item.id)
}

export const decreaseItem = (cartItems, item) => {
  return cartItems.map(cartItem =>
    cartItem.id === item.id ?
      { ...cartItem, qty: cartItem.qty - 1 } :
      cartItem
  )
}

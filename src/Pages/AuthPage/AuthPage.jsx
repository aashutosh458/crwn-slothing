import React from 'react'
import SignIn from '../../Components/SignIn/SignIn'
import SignUp from '../../Components/SignUp/SignUp'

import './AuthPage.scss'

const AuthPage = () => {

  return (
    <div className='auth'>
      <SignIn />
      <SignUp />
    </div>
  )
}

export default AuthPage
import { connect } from 'react-redux'
import CheckoutItem from '../../Components/checkoutItem/checkoutItem'
import { selectCartItems, selectCartItemsPrice } from '../../redux/cart/cartSelector'

import './checkout.scss'
import StripeButton from '../../Components/StripeButton/StripeButton'

const checkout = ({ cartItems, totalPrice }) => {

  return (
    <div className='checkout-page'>
      <div className="checkout-header">
        <div className="header-block">
          <span>Product</span>
        </div>
        <div className="header-block">
          <span>Description</span>
        </div>
        <div className="header-block">
          <span>Qty</span>
        </div>
        <div className="header-block">
          <span>Price</span>
        </div>
        <div className="header-block">
          <span>Remove</span>
        </div>
      </div>
      {cartItems.map(cartItem => {
        return (
          <CheckoutItem key={cartItem.id} cartItem={cartItem} />
        )
      })}
      <div className="total">
        <span>
          Total: ${totalPrice}
        </span>
      </div>
      <div className="test-warning">
        *Please use the following cc for payments*
        <br />
        4242 4242 4242 4242 - Exp: 01/20 - CVV: 123
      </div>
      <StripeButton price={totalPrice} />
    </div>
  )
}

const mapStateToProps = (state) => ({
  cartItems: selectCartItems(state),
  totalPrice: selectCartItemsPrice(state)
})

export default connect(mapStateToProps)(checkout);
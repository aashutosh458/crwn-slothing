import './Collection.scss'
import { connect } from 'react-redux'
import { selectCollection } from '../../redux/shop/shopSelector'
import ItemCard from '../../Components/ItemCard/ItemCard'

const Collection = ({ collection }) => {
  console.log(collection)
  const { title, items } = collection
  return (
    <div className='collection-page'>
      <h2 className='title'>{title}</h2>
      <div className="items">
        {
          items.map(item => {
            return (
              <ItemCard item={item} key={item.id} />
            )
          })
        }
      </div>
    </div>
  )
}

const mapStateToProps = (state) => ({
  collection: selectCollection(window.location.pathname.split('/')[2])(state)
})

export default connect(mapStateToProps)(Collection);
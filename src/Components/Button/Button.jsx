import React from 'react'

import './Button.scss'

const Button = ({ inverted, children, isGoogleSignIn, ...otherProps }) => {
  return (
    <button
      {...otherProps}
      className={`${inverted ? 'inverted' : ''} ${isGoogleSignIn ? 'google-sign-btn' : ''} custom-btn`}
    >
      {children}
    </button>
  )
}

export default Button
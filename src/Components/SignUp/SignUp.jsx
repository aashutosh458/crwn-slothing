import React, { Component } from 'react'
import Button from '../Button/Button';
import Input from '../Input/Input';

import { auth, createUserProfileDocument } from '../../Firebase/firebase.utils';

import './SignUp.scss'

export default class SignUp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      displayName: '',
      email: '',
      password: '',
      confirmPassword: ''
    }
  }

  handleChage = (e) => {
    const { name, value } = e.target
    this.setState({ [name]: value });
  }
  handleSubmit = async (e) => {
    e.preventDefault();
    const { displayName, email, password, confirmPassword } = this.state;
    if (password !== confirmPassword) {
      alert('password dont match')
      return;
    }

    try {
      const { user } = await auth.createUserWithEmailAndPassword(email, password);
      await createUserProfileDocument(user, { displayName });
      this.setState({ displayName: '', email: '', password: '', confirmPassword: '' })

    } catch (error) {
      console.error(error);
    }
  }

  render() {
    return (
      <div className='sign-in'>
        <h2>
          I do not have an account
        </h2>
        <span> sign up with your email and password</span>
        <form
          onSubmit={this.handleSubmit}>
          <Input
            label={'Display Name'}
            name='displayName'
            type='text'
            value={this.state.displayName}
            handleChage={this.handleChage}
            required />
          <Input
            label={'Email'}
            name='email'
            type='email'
            value={this.state.email}
            handleChage={this.handleChage}
            required />
          <Input
            label={'Password'}
            name='password'
            type='password'
            value={this.state.password}
            handleChage={this.handleChage}
            required />
          <Input
            label={'Confirm Password'}
            name='confirmPassword'
            type='password'
            value={this.state.confirmPassword}
            handleChage={this.handleChage}
            required />
          <div className='buttons'>
            <Button
              type={'submit'}
            >
              Sign up
            </Button>
          </div>

        </form>
      </div>
    )
  }
}

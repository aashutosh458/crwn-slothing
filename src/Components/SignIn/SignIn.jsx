import React, { Component } from 'react'
import Button from '../Button/Button';
import Input from '../Input/Input';

import { auth, signInWithGoogle } from '../../Firebase/firebase.utils';

import './SignIn.scss'

export default class SignIn extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: ''
    }
  }

  handleSubmit = async (e) => {
    e.preventDefault();
    const { email, password } = this.state;

    try {
      await auth.signInWithEmailAndPassword(email, password);
      this.setState({ email: '', password: '' });
    } catch (error) {
      alert(error.message)
    }
  }

  handleChage = (e) => {
    const { name, value } = e.target
    this.setState({ [name]: value });
  }

  render() {
    return (
      <div className='sign-in'>
        <h2>
          I already have an account
        </h2>
        <span> sign in with your email and password</span>
        <form
          onSubmit={this.handleSubmit}>
          <Input
            label={'Email'}
            name='email'
            type='email'
            value={this.state.email}
            handleChage={this.handleChage}
            required
          />
          <Input
            label={'Password'}
            name='password'
            type='password'
            value={this.state.password}
            handleChage={this.handleChage}
            required />
          <div className='buttons'>
            <Button
              type={'submit'}
            >
              Sign in
            </Button>
            <Button
              type={'submit'}
              onClick={signInWithGoogle}
              isGoogleSignIn
            >
              Sign in with Google
            </Button>
          </div>
        </form>
      </div>
    )
  }
}

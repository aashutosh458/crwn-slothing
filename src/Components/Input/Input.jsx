import React from 'react'

import './Input.scss'

const Input = ({ handleChage, label, ...otherProps }) => {
  return (
    <div className='group'>
      <input
        onChange={handleChage}
        {...otherProps}
      />
      {
        label ? <label className={`${!otherProps.value.lenght ? 'shrink' : ''} label`}> {label}</label> : null
      }

    </div>
  )
}

export default Input
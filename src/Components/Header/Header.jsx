import { Link } from 'react-router-dom'
import { ReactComponent as Logo } from '../../Assets/Logo/Logo.svg'
import { connect } from 'react-redux'
import { auth } from '../../Firebase/firebase.utils'
import { toggleCartHidden } from '../../redux/cart/cartAction'
import { selectCartHidden } from '../../redux/cart/cartSelector'
import { selectCurrentUser } from '../../redux/user/userSelector'
import { createStructuredSelector } from 'reselect'

import IconCart from '../Icon/IconCart'
import CartDropdown from '../CartDropdown/CartDropdown'

import './Header.scss'

const Header = ({ currentUser, toggleCartHidden, hidden }) => {

  return (
    <div className='header'>
      <Link to='/'>
        <Logo />
      </Link>
      <div className='options'>
        <Link to={'./shop'}>
          SHOP
        </Link>
        <Link>
          CONTACT
        </Link>
        {
          currentUser ?
            <Link onClick={() => auth.signOut()}>
              SIGN OUT
            </Link>
            :
            <Link to={'./auth'}>
              SIGN IN
            </Link>
        }

        <Link onClick={toggleCartHidden}>
          <IconCart />
        </Link>
      </div>
      {
        !hidden && <CartDropdown />
      }

    </div>
  )
};

const mapStateToProps = createStructuredSelector({
  currentUser: selectCurrentUser,
  hidden: selectCartHidden
});

const mapDispatchToProps = dispatch => ({
  toggleCartHidden: () => dispatch(toggleCartHidden())
})

export default connect(mapStateToProps, mapDispatchToProps)(Header);

// export default Header;
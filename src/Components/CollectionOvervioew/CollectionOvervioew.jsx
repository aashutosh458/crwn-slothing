import './CollectionOvervioew.scss'
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { selectCollectionForPreview } from '../../redux/shop/shopSelector';

import PreviewCollection from '../PreviewCollection/PreviewCollection'

const CollectionOvervioew = ({ collections }) => {
  return (
    <div className='collections-overview'>
      {
        collections.map(({ id, ...otherprops }) => {
          return (
            <PreviewCollection key={id} {...otherprops} />
          );
        })
      }
    </div>
  )
}
const mapStateToProps = createStructuredSelector({
  collections: selectCollectionForPreview
})

export default connect(mapStateToProps)(CollectionOvervioew);
import { useNavigate } from 'react-router-dom';

import './MenuItem.scss'

const MenuItem = ({ title, imageUrl, linkUrl }) => {
  const navigate = useNavigate();
  return (
    <div className="content-card " onClick={() => { navigate(`/${linkUrl}`) }}>
      <img src={imageUrl} className="content-card-img" alt="..." />
      <div className="card-text">
        <h1>{title}</h1>
        <p >Shop Now</p>
      </div>
    </div>
  )
}

export default MenuItem;
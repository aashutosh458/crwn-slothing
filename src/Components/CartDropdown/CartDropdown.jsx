import Button from '../Button/Button'
import CartItem from '../CartItem/CartItem'

import { connect } from 'react-redux'
import { selectCartItems } from '../../redux/cart/cartSelector'
import { useNavigate } from 'react-router-dom';
import { toggleCartHidden } from '../../redux/cart/cartAction';

import './CartDropdown.scss'

const CartDropdown = ({ cartItems, dispatch }) => {

  const navigate = useNavigate();

  return (
    <div className='cart-dropdown'>
      {cartItems?.length ?
        <>
          <div className='cart-items'>
            {cartItems.map((item) => <CartItem key={item.id} item={item} />)}
          </div>
          <Button
            onClick={() => {
              navigate(`/checkout`);
              dispatch(toggleCartHidden());
            }}
          >
            Checkout
          </Button>
        </>
        :
        <span className='empty-message'>
          Your Cart is empty
        </span>
      }
    </div >
  )
}

const mapStateToProps = (state) => ({
  cartItems: selectCartItems(state)
})

export default connect(mapStateToProps)(CartDropdown);
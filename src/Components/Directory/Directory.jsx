import MenuItem from '../MenuItem/MenuItem';

import { connect } from 'react-redux';
import { selectDirectorySections } from '../../redux/directory/directorySelector';
import { createStructuredSelector } from 'reselect';

import './Directory.scss'

const Directory = ({ sections }) => {
  return (
    <div className='directory-menu'>
      {
        sections.map(({ id, ...directoryProps }) => {
          return (
            <MenuItem key={id} {...directoryProps} />
          );
        })
      }
    </div>
  )
}

const mapStateToProps = createStructuredSelector({
  sections: selectDirectorySections
})

export default connect(mapStateToProps)(Directory);
import { connect } from 'react-redux'
import { clearItemFromCart, reduceQty, addItem } from '../../redux/cart/cartAction';

import './checkoutItem.scss'

const CheckoutItem = ({ cartItem, clearItem, addItem, reduceQty }) => {
  const { name, imageUrl, price, qty } = cartItem;
  return (
    <div className='checkout-item'>
      <div className="image-container">
        <img src={imageUrl} alt='item' />
      </div>
      <span className="name">
        {name}
      </span>
      <span className="qty">
        <span className='inc-dec' onClick={() => qty === 1 ? clearItem(cartItem) : reduceQty(cartItem)}>-</span>
        {qty}
        <span className='inc-dec' onClick={() => addItem(cartItem)}>+</span>
      </span>
      <span className="price">
        {price}
      </span>
      <span onClick={() => { clearItem(cartItem) }} className="remove-button">X</span>
    </div>
  )
}

const mapDispatchToProps = dispatch => (
  {
    clearItem: item => dispatch(clearItemFromCart(item)),
    addItem: item => dispatch(addItem(item)),
    reduceQty: item => dispatch(reduceQty(item))
  }
)

export default connect(null, mapDispatchToProps)(CheckoutItem);
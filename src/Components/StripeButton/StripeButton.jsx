import React from 'react'

import StripeCheckout from 'react-stripe-checkout'

const StripeButton = ({ price }) => {
  const priceForStripe = price * 100;
  const publishablekey = 'pk_test_51Mt8H5SC6KY75Qzp6v6aTBJoQGJpKXod7tK9ZtUvGKd6e8kSQRj5FgBamxHkbSixb1mYQWWrSGRlUXPBdLm0PiBF00yfSF56fE'

  const onToken = token => {
    console.log(token);
    alert('Payment Successful')
  }

  return (
    <StripeCheckout
      label='Pay Now'
      name='CRWN CLothing Ltd.'
      billingAddress
      shippingAddress
      image='https://images.unsplash.com/photo-1606663889134-b1dedb5ed8b7?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=387&q=80'
      description={`your total is $${price}`}
      amount={priceForStripe}
      panelLabel='Pay Now'
      token={onToken}
      stripeKey={publishablekey}
    />
  )
}

export default StripeButton;  
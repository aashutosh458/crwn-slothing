import React from 'react'
import Button from '../Button/Button';

import { connect } from 'react-redux';
import { addItem } from '../../redux/cart/cartAction';

import './ItemCard.scss'

const ItemCard = ({ item, addItem }) => {
  const { imageUrl, price, name } = item
  return (
    <div className='item-card'>
      <img src={imageUrl} alt={name} />
      <div>
        <h4> {name}</h4>
        <h5>${price} </h5>
      </div>
      <div className='button'>
        <Button inverted onClick={() => addItem(item)} >
          ADD TO CART
        </Button>
      </div>
    </div>
  );
}

const mapDispatchToProps = dispatch => ({
  addItem: item => dispatch(addItem(item))
})

export default connect(null, mapDispatchToProps)(ItemCard);